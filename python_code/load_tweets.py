import argparse
import json
from copy import copy

import sqlite3


class DataBase:

    def __init__(self, db_name='task.db', afinn=None):
        self._db_name = db_name
        self._afinn = {} if afinn is None else afinn

    def connect(self):
        self.connection = sqlite3.connect(self._db_name)

    def close(self):
        self.connection.close()

    def execute(self, query):
        self.connection.execute(query)
        self.connection.commit()

    def create_tweets_table(self):
        self.execute('''CREATE TABLE tweets 
                        (name TEXT, tweet_text TEXT, country_code TEXT, 
                        display_url TEXT, lang TEXT, created_at TEXT, 
                        location TEXT, tweet_sentiment INTEGER);''')

    @staticmethod
    def _transform_to_str(tweet):
        return '("{name}", "{tweet_text}", "{country_code}", "{display_url}", "{lang}", "{created_at}", "{location}", {tweet_sentiment})'.format(
            name=tweet.get('name', 'NULL'),
            tweet_text=tweet.get('text', 'NULL'),
            country_code=tweet.get('country_code', 'NULL'),
            display_url=tweet.get('display_url', 'NULL'),
            lang=tweet.get('lang', 'NULL'),
            created_at=tweet.get('created_at', 'NULL'),
            location=tweet.get('location', 'NULL'),
            tweet_sentiment=tweet.get('tweet_sentiment', 0),
        )

    @staticmethod
    def _validate(tweet):
        return True

    @staticmethod
    def split_text(text):
        return text.split(' ')
    
    def add_tweet_sentiment(self, tweet):
        tweet_sentiment = 0
        for word in self.split_text(tweet.get('text', '')):
            tweet_sentiment += self._afinn.get(word, 0)
        
        tweet = copy(tweet)
        tweet['tweet_sentiment'] = tweet_sentiment
        return tweet


    def insert_tweets(self, tweets, validate=False):
        if validate:
            tweets = filter(self._validate, tweets)
        tweets = map(self.add_tweet_sentiment, tweets)
        tweets = list(map(self._transform_to_str, tweets))

        # Некоторые твиты содержат ошибки, с которыми я пока не разобрался
        #
        #self.execute('''INSERT INTO tweets 
        #    (name, tweet_text, country_code, display_url, lang, created_at, location, tweet_sentiment)
        #    VALUES ''' + ', '.join(tweets[:25]) + ';')
        
        for tweet in tweets:
            try:
                self.execute('''INSERT INTO tweets 
                    (name, tweet_text, country_code, display_url, lang, created_at, location, tweet_sentiment)
                    VALUES  {tweet};'''. format(tweet=tweet))
            except sqlite3.OperationalError:
                print(sqlite3.OperationalError)
                print("problem with {tweet}".format(tweet=tweet))
        self.connection.comit()
      


def load_tweets(tweets_file, affin_file, db_name='task.db'):
    with open(tweets_file) as file:
        tweets = list(map(json.loads, file.readlines()))
    with open(affin_file) as file:
        afinn = {tpl[0]: int(tpl[1]) for tpl in map(lambda line: line.split('\t'), file.readlines())}

    db = DataBase(db_name=db_name, afinn=afinn)
    db.connect()
    db.insert_tweets(tweets)
    db.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--tweets_file', required=True)
    parser.add_argument('--affin_file', required=True)
    parser.add_argument('--db_name', required=False, default='task.db')
    args = parser.parse_args()

    load_tweets(args.tweets_file, args.affin_file, args.db_name)
